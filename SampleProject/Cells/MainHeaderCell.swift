//
//  MainHeaderCell.swift
//  SampleProject
//
//  Created by Alex on 21.03.18.
//  Copyright © 2018 Oleksandr Atamanskyi. All rights reserved.
//

import UIKit

class MainHeaderCell: UITableViewCell {
    @IBOutlet weak var headerTitle: UILabel!
}
