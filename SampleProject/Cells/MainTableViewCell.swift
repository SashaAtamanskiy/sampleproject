//
//  MainTableViewCell.swift
//  SampleProject
//
//  Created by Oleksandr Atamanskyi on 3/20/18.
//  Copyright © 2018 Oleksandr Atamanskyi. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private lazy var dataSource = MainCollectionViewDataSource(collectionView: self.collectionView)
    
    func setCollectionViewDataSourceDelegate(forRow row: Int, data: [SampleModel]) {
        collectionView.delegate = dataSource
        collectionView.dataSource = dataSource
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        dataSource.reload(with: data)
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
    
}
