//
//  ViewController.swift
//  SampleProject
//
//  Created by Oleksandr Atamanskyi on 3/20/18.
//  Copyright © 2018 Oleksandr Atamanskyi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private lazy var tableViewDataSource = MainTableViewDataSource(tableView: self.tableView)
    
    var headerText: String?
    
    // Test data
    let sample = [
        //fisrt model
        [SampleModel(image: #imageLiteral(resourceName: "1"), title: "Backpack"),
         SampleModel(image: #imageLiteral(resourceName: "2"), title: "Bottle"),
         SampleModel(image: #imageLiteral(resourceName: "3"), title: "Pack Pride")],
        [SampleModel(image: #imageLiteral(resourceName: "4"), title: "A man"),
         SampleModel(image: #imageLiteral(resourceName: "5"), title: "Shoes"),
         SampleModel(image: #imageLiteral(resourceName: "6"), title: "Condom")],
        [SampleModel(image: #imageLiteral(resourceName: "7"), title: "Gold pack"),
         SampleModel(image: #imageLiteral(resourceName: "8"), title: "Details"),
         SampleModel(image: #imageLiteral(resourceName: "9"), title: "A bag")]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = tableViewDataSource
        tableView.delegate = tableViewDataSource
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableViewDataSource.reload(with: sample, headerText: headerText ?? "")
    }

}

