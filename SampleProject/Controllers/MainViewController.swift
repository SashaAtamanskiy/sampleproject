//
//  MainViewController.swift
//  SampleProject
//
//  Created by Alex on 21.03.18.
//  Copyright © 2018 Oleksandr Atamanskyi. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func goToNextController(_ sender: Any) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TableViewController") as! ViewController
        viewController.headerText = textField.text
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
}
