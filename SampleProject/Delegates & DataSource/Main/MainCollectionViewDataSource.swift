//
//  MainCollectionViewDataSource.swift
//  SampleProject
//
//  Created by Oleksandr Atamanskyi on 3/20/18.
//  Copyright © 2018 Oleksandr Atamanskyi. All rights reserved.
//

import UIKit

final class MainCollectionViewDataSource: NSObject {
    
    weak var collectionView: UICollectionView!
    
    private var data: [SampleModel] = []
    
    init(collectionView: UICollectionView) {
        super.init()
        
        self.collectionView = collectionView
    }
    
    func reload(with data: [SampleModel]) {
        self.data = data
        self.collectionView.reloadData()
    }
    
}

extension MainCollectionViewDataSource: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCell", for: indexPath) as! MainCell
        cell.setupCell(with: data[indexPath.row])
        return cell
    }
    
}

extension MainCollectionViewDataSource: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did select on \(data[indexPath.row].title)")
    }
    
}
