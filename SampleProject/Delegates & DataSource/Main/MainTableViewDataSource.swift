
//
//  MainTableViewDataSource\.swift
//  SampleProject
//
//  Created by Oleksandr Atamanskyi on 3/20/18.
//  Copyright © 2018 Oleksandr Atamanskyi. All rights reserved.
//

import UIKit

final class MainTableViewDataSource: NSObject {
    
    weak var tableView: UITableView!
    
    private var storedOffsets = [Int: CGFloat]()
    private var data: [[SampleModel]] = []
    private let headerCellIdentifier = "headerIdentifier"
    private var headerText: String?
    
    init(tableView: UITableView) {
        self.tableView = tableView
        
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedSectionHeaderHeight = 44
    }
    
    func reload(with data: [[SampleModel]], headerText: String) {
        self.data = data
        self.headerText = headerText
        self.tableView.reloadData()
    }
    
}

extension MainTableViewDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell") as! MainTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? MainTableViewCell else {
            return
        }
        cell.setCollectionViewDataSourceDelegate(forRow: indexPath.row, data: self.data[indexPath.row])
        cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? MainTableViewCell else {
            return
        }
        storedOffsets[indexPath.row] = cell.collectionViewOffset
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: headerCellIdentifier) as! MainHeaderCell
        header.headerTitle.text = self.headerText
        return header
    }
    
}

extension MainTableViewDataSource: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
}
