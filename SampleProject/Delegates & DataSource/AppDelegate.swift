//
//  AppDelegate.swift
//  SampleProject
//
//  Created by Oleksandr Atamanskyi on 3/20/18.
//  Copyright © 2018 Oleksandr Atamanskyi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

}

