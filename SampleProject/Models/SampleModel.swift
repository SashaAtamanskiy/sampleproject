//
//  SampleModel.swift
//  SampleProject
//
//  Created by Oleksandr Atamanskyi on 3/20/18.
//  Copyright © 2018 Oleksandr Atamanskyi. All rights reserved.
//

import UIKit

struct SampleModel {
    var image: UIImage?
    var title: String
}
